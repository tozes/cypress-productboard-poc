Cypress.Commands.add('loginToProductboard', (username='REDACTED', password='REDACTED') => {
    // (TODO) Implement using cy.request() to increase init speed
    cy.visit('/')
    cy.get('#email')
      .type(username, {force: true})
    cy.get('#password')
      .type(password, {force: true})
      .type('{enter}', {force: true})
})
