describe('Landing page tests', () => {

  before(() => {
    cy.visit('/')
  })

  context('Landing page contents', () => {
    it('Has the company logo', () => {
      cy.get('.brand')
        .parent()
        .should('have.attr', 'href', 'http://www.productboard.com')
    })

    it('Has login with Google button', () => {
      cy.get('.googleButton')
        .children('a')
        .should('have.attr', 'href', '/users/auth/google_oauth2')
    })

    it('Has sign up link', () => {
      cy.get('.login-message')
        .should('contain', 'Don\'t have an account')
        .children('a')
        .should('have.attr', 'href', '/register')
    })

    it('Has password recovery link', () => {
      cy.get('.forgotten-password')
        .children('a')
        .should('contain', 'Forgot your password?')
        .should('have.attr', 'href', '/password_resets/new')
    })

    it('Has foot note with links', () => {
      cy.get('.footer')
        .then(($footer) => {
          cy.wrap($footer).should('contain', 'Copyright')
          cy.wrap($footer).contains('Privacy')
            .should('have.attr', 'href', 'https://www.productboard.com/privacy-policy')
          cy.wrap($footer).contains('Terms')
            .should('have.attr', 'href', 'https://www.productboard.com/terms-of-service')

        })
    })
  })

  context('Signing in', () => {
    it('Displays an error message on failed login', () => {
      cy.server()
      cy.route({
          method: 'POST',
          url: '/users/sign_in',
          status: 401,
          response: {'error': 'You shall not pass'},
      })
      cy.loginToProductboard()
      cy.get('.error')
        .should('contain', 'You shall not pass')
    })

    it('Successful login using form redirects to the inbox', () => {
      cy.loginToProductboard()
      cy.url().should('include', 'inbox')
    })

  })

})
