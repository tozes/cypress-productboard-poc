describe('Smoke tests', () => {

  before(() => {
    cy.loginToProductboard()
  })

  context('App contents', () => {
      it('Sidebar contains bottuns for all components', () => {
        cy.wrap(['.inbox', '.features', '.roadmap', '.portal'])
          .each((locator => {
              cy.get(locator)
          }))
      })

    it('Check that the contents of the features module', () => {
        cy.get('.features').click()
        cy.url().should('include', 'feature-board')
        // (TODO) check existance of particular elements (skipping due of CSS class  obfuscation)
    })
  })
})
